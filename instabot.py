import autoit
import config
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep

# Nice information to make it cleaner
# https://github.com/timgrossmann/InstaPy/blob/master/instapy/settings.py

# Reminder
# Xpath=//tagname[@attribute='value']


# TODO transform in inputs
# image_path = r"D:\repositories\Selenium-Python-Boilerplate\mytests\pictures\test.jpg"
# i_caption = 'This is a test #wtfhehehehe'
# i_location = ''
# i_friends = ''

class TestChrome():
    def setup(self):
        mobile_emulation = { "deviceName": "Pixel 2" }
        opts = webdriver.ChromeOptions()
        opts.add_experimental_option("mobileEmulation", mobile_emulation)
        self.driver = webdriver.Chrome(chrome_options=opts)

    def close(self):
        self.driver.quit()

class TestHeadless():
    def setup(self):
        # Windows
        self.driver = webdriver.PhantomJS("C://phantomjs/bin/phantomjs.exe")
        # Mac / Linux
        # self.driver = webdriver.PhantomJS()

    def close(self):
        self.driver.quit()

def captionToArray(i_caption):
    caption_array = i_caption.split("/n")
    for index, caption_part in enumerate(caption_array):
        caption_array[index] = caption_part.split("/r")
    return caption_array

def login(uploader, username, password):
    driver = uploader.driver
    # Get on the instagram page
    driver.get("https://www.instagram.com/accounts/login/?source=auth_switcher")
    sleep(2)
    # Connection with facebook
    fb_connection_element = driver.find_element_by_xpath('//*[text() = "Continuer avec Facebook"]')
    fb_connection_element.click()
    sleep(2)
    # Input email
    email_element = driver.find_element_by_xpath('//*[@id="m_login_email"]')
    email_element.send_keys(username)
    # Input password
    password_element = driver.find_element_by_xpath('//*[@id="m_login_password"]')
    password_element.send_keys(password)
    # Connection button
    connection_element = driver.find_element_by_xpath('//*[@id="u_0_4"]/button')
    connection_element.click()
    return

def validatePhotoPositioning(uploader):
    driver = uploader.driver
    sleep(2)
    next_button = driver.find_element_by_xpath('//*[text() = "Suivant"]')
    next_button.click()
    return

def wroteCaption(uploader, i_caption):
    driver = uploader.driver
    sleep(2)
    caption_element = driver.find_element_by_xpath("//textarea")
    caption_array = captionToArray(i_caption)
    for i, element in enumerate(caption_array):
        for j, inner_element in enumerate(element):
            caption_element.send_keys(inner_element)
            if (len(element) > 1 and j != len(element)-1):
                caption_element.send_keys(Keys.ENTER)
        if (len(caption_array) > 1 and i != len(caption_array)-1):
            caption_element.send_keys(Keys.ENTER)
            caption_element.send_keys(Keys.ENTER)
    return

def selectLocation(uploader, i_location):
    driver = uploader.driver
    return

def selectFriend(uploader, i_friends):
    driver = uploader.driver
    return

def validatePhotoInformation(uploader):
    driver = uploader.driver
    share_button = driver.find_element_by_xpath('//*[text() = "Partager"]')
    share_button.click()
    return

def uploadPhoto(uploader, root_dir, image_path, i_caption, i_location, i_friends):
    driver = uploader.driver
    new_post_btn = driver.find_element_by_xpath("//div[@role='menuitem']").click()
    sleep(1.5)

    autoit.win_active("Ouvrir")
    sleep(2)
    image_full_path = root_dir + '\\' + image_path
    image_full_path = image_full_path.replace('/', '\\')
    autoit.control_send("Ouvrir","Edit1", image_full_path)
    sleep(1.5)
    autoit.control_send("Ouvrir","Edit1", "{ENTER}")

    validatePhotoPositioning(uploader)
    wroteCaption(uploader, i_caption)
    selectLocation(uploader, i_location)
    selectFriend(uploader, i_friends)
    # validatePhotoInformation(uploader)

    return


def closeAlert(uploader, alertText):
    driver = uploader.driver
    sleep(8)
    alert_element = driver.find_element_by_xpath('//*[text() = "{}"]'.format(alertText))
    alert_element.click()
    return


def main(param_1, param_2, param_3, param_4, param_5):

    # Params
    root_dir = param_1
    image_path = param_2
    i_caption = param_3
    i_location = param_4
    i_friends = param_5

    # Instantiate the wanted driver
    uploader = TestChrome()
    # uploader = TestHeadless()
    uploader.setup()

    # Login
    login(uploader, config.username, config.password)

    # Close alert
    closeAlert(uploader, 'Annuler')

    # Upload the photo provided by script argument
    uploadPhoto(uploader, root_dir, image_path, i_caption, i_location, i_friends)

    # Close alert 2
    # closeAlert(uploader, 'Plus tard')

    # Close the browser/driver
    # Comment during your tests
    # uploader.close()


### Main part of the script
if __name__ == '__main__':
    param_1 = sys.argv[1]
    param_2 = sys.argv[2]
    param_3 = sys.argv[3]
    param_4 = sys.argv[4]
    param_5 = sys.argv[5]
    main(param_1, param_2, param_3, param_4, param_5)