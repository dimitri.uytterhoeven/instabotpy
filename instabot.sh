#!/bin/sh

## FUNCTIONS
# Parameter 1 : timestamp
# Parameter 2 : data to match
are_dates_matching () {
  # First argument should be the date to match
  TIMESTAMP=$1
  date_to_match=$2
  if [[ -z "${date_to_match}" ]]; then
    echo -e $TIMESTAMP "ERROR: no date to match"
  fi
  today=$(date +"%Y%m%d%H%M")

  if [ "$date_to_match" == "$today" ]; then
    echo -e $TIMESTAMP "INFO: Dates match! A photo will be uploaded"
    # 0 = true
    return 0
  else
    echo -e $TIMESTAMP "INFO: Dates are not matching, no upload"
    # 1 = false
    return 1
  fi
}
# Parameter 1: array with dates to modify
update_posted_lines () {
  dates_to_update=$1
  now=$(date +"%Y%m%d%H%M")
  for i in ${dates_to_update[@]}
  do
    perl -pi.bak -e "s/${i}/done-${i}/g" "${rootDir}/data/planning.csv"
  done
}

## CONSTANTS
# Current directory
cd "$(dirname "$0")"
rootDir=`pwd`


# Infinite loop
while true
do

  # Array dates to initialize
  dates_to_update=()

  # Retrieve the timestamp
  TIMESTAMP=$(date +"%Y%m%d_%H-%M-%S")
	echo -e $TIMESTAMP "Still running!"

  # Retrieve the planning with
  # - the date at which we would like to post the image
  # - the image path
  # - the caption for the selected image
  while IFS=, read -r postingdate imagepath caption
  do
      echo -e $TIMESTAMP "I got:$postingdate|$imagepath|$caption"
      if are_dates_matching $TIMESTAMP $postingdate; then
        dates_to_update+=("${postingdate}")
        echo -e $TIMESTAMP "Call python...";
        # Call the instagram python script when a date is matching
        python instabot.py $rootDir $imagepath "$caption" ' ' ' '
      else
        echo -e $TIMESTAMP "Do nothing";
      fi
  done < "${rootDir}/data/planning.csv"

  # Archives posts that are outdated
  update_posted_lines $dates_to_update

	sleep 30

done